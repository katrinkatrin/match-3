local model = require('model')
local controller = require('controller')

model.init()

local example = io.read()

temp = {}

for i in string.gmatch(example, "%S+") do
   table.insert(temp, i)
end

local command = temp[1]
local x = tonumber(temp[2])
local y = tonumber(temp[3])
local direction = temp[4]

while(command ~= 'q') do
	if command == 'm' then

		if x <= 9 and x >= 0 and y <= 9 and y >= 0 then
			from = {x=temp[2], y=temp[3]}
			to = controller.getTo(from, direction)

			if to.x ~= -1 and to.y ~= -1 then
				model.move(from, to)
			else
				print('Incorrect direction!')
			end
		else
			print('Incorrect coordinates!')
		end
	else
		print('Incorrect command!')
	end


	example = io.read()
	local j = 1
	for i in string.gmatch(example, "%S+") do
   		temp[j] = i
   		j = j+1
	end
	command = temp[1]
	x = tonumber(temp[2])
	y = tonumber(temp[3])
	direction = temp[4]

end

