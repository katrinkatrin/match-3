local controller = {}

function controller.getTo(from, direction)
	print(from.x)
	print(from.y)
	print(direction)

	to = {}
	if direction == 'l' then
		if from.x == '0' then
			to.x = -1
			to.y = -1
			return to
		end
		to.x = from.x - 1
		to.y = from.y 

	elseif direction == 'r' then
		if from.x == '9' then
			to.x = -1
			to.y = -1
			return to
		end
		to.x = from.x + 1
		to.y = from.y
	
	elseif direction == 'u' then
		if from.y == '0' then
			to.x = -1
			to.y = -1
			return to
		end
		to.x = from.x
		to.y = from.y - 1
	
	elseif direction == 'd' then
		if from.y == '9' then
			to.x = -1
			to.y = -1
			return to
		end
		to.x = from.x
		to.y = from.y + 1
	end

	return to
end


return controller