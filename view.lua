local view = {}

function view.drawData(data)
	local size = 10

	print("    0 1 2 3 4 5 6 7 8 9")
	print("   --------------------")

	for j = 0, size-1, 1 do
		io.write(j .. ' | ')
		for i = 1, size, 1 do
			--local a = data[10*j + i] 
			io.write(data[10*j + i])
			io.write(' ')
   		end
   		io.write('\n')
	end

end

return view