local model = {}

local view = require('view')

local colors = {'A', 'B', 'C', 'D', 'E', 'F'}

local data = {}

local function isCombination()
	combination = false
	--горизонталь
	for j = 0, 9 do
		for i = 1, 9 do
			if data[j*10 + i] == data[j*10 + i + 1] then
				if data[j*10 + i] == data[j*10 + i - 2] or
					data[j*10 + i] == data[j*10 + i + 3] or
					data[j*10 + i] == data[(j-1)*10 + i - 1] or
					data[j*10 + i] == data[(j+1)*10 + i - 1] or
					data[j*10 + i] == data[(j-1)*10 + i + 2] or
					data[j*10 + i] == data[(j+1)*10 + i + 2]  then
						combination = true
				end
			end
		end
	end
	--
	--вертикаль
	for j = 0, 8 do
		for i = 1, 10 do
			if data[j*10 + i] == data[(j+1)*10 + i] then
				if data[j*10 + i] == data[(j+3)*10 + i] or
					data[j*10 + i] == data[(j-2)*10 + i] or
					data[j*10 + i] == data[(j-1)*10 + i - 1] or
					data[j*10 + i] == data[(j-1)*10 + i + 1] or
					data[j*10 + i] == data[(j+2)*10 + i + 1] or
					data[j*10 + i] == data[(j+2)*10 + i - 1]  then
						combination = true
				end
			end
		end
	end
return combination
end

local function isNull()
	null = false
	for j = 9, 1, -1 do
		for i = 10, 1, -1 do
			if data[j*10 + i] == '0'  and data[(j-1)*10 + i] ~= '0' then
				null = true
			end
		end
	end
	return null
end

local function fillNewValues()
	for j = 0, 9 do
		for i = 1, 10 do
			if data[j*10 + i] == '0' then
				data[j*10 + i] = colors[math.random(1,6)]
			end
		end
	end
end 

local function moveElements()
	for j = 9, 1, -1 do
		for i = 10, 1, -1 do
			if data[j*10 + i] == '0' then
				data[j*10 + i] = data[(j-1)*10 + i]
				data[(j-1)*10 + i] = '0'
			end
		end
	end
end

local function deleteMatch()
	isMatch = false
	local dataToMatch = { }
	--проверка по горизонтали
	for j = 0, 9 do
		for i = 1, 8 do
			if data[j*10 + i] == data[j*10 + i + 1] and data[j*10 + i + 1] == data[j*10 + i + 2] then
				table.insert(dataToMatch, j*10 + i)
				table.insert(dataToMatch, j*10 + i + 1)
				table.insert(dataToMatch, j*10 + i + 2)
				print('detected Horiz'..i..' '..j)
			end
		end
	end
	--проверка по вертикали
	for i = 1, 10 do
		for j = 0, 7 do
			if data[j*10 + i] == data[(j+1)*10 + i] and data[(j+1)*10 + i] == data[(j+2)*10 + i] then
				table.insert(dataToMatch, j*10 + i)
				table.insert(dataToMatch, (j + 1) *10 + i)
				table.insert(dataToMatch, (j + 2) *10 + i)
				print('detected Vert'..i..' '..j)
			end

		end
	end

	local count = #dataToMatch
	if count > 0 then
		isMatch = true
	end

	for i = 1, #dataToMatch, 1 do
		data[dataToMatch[i]] = '0'
	end

	while(isNull()) do
		moveElements()
	end

	return isMatch
end


local function setMix()
	math.randomseed(os.time())
	fromX = math.random(1,10)
	fromY = math.random(0,9)
	toX = math.random(1,10)
	toY = math.random(0,9)
	temp = data[fromY*10 + fromX]
	data[fromY*10 + fromX] = data[toY*10 + toX]
	data[toY*10 + toX] = temp
end


--/////////////////////////

function model.init()
	math.randomseed(os.time())
	for i = 1, 100, 1 do
		data[i] = colors[math.random(1,6)]
	end

	while deleteMatch() do
		fillNewValues()
	end

	model.tick()
end

function model.move(from, to)	
	from.x = from.x + 1
	to.x = to.x + 1

	tempValue = data[to.y*10 + to.x]
	data[to.y*10 + to.x] = data[from.y*10 + from.x]
	data[from.y*10 + from.x] = tempValue

	model.tick()
	while deleteMatch() do
		fillNewValues()
		model.tick()
	end
end

function model.mix()
	while (isCombination()~=true) do
		setMix()
		model.dump()
	end
end

function model.tick()
	model.dump()
end

function model.dump()
	view.drawData(data)
end


return model